<?php
/**
 * Site time stats.
 *
 * @package ElggGraphStats
 */

elgg_load_library('elgg:graphs:timestats');

$title = elgg_echo('graphstats:timestats');

elgg_push_breadcrumb($title);
	
$type = get_input('type', 'user');
$subtype = get_input('subtype', '');
$filter = get_input('filter', 'absolute');
$type_subtype = $type . ($subtype?":$subtype":"");

$table = elgg_view('graphs/data/timestats', array(
	'type' => $type,
	'subtype' => $subtype,
	'relative' => ($filter == 'relative'),
));

$content = elgg_view('graphs/timestats', array('table' => $table));

$filter = elgg_view('graphstats/timestats_filter_menu', array('selected' => $relative));

timestats_setup_sidebar_menu();

$body = elgg_view_layout('content', array(
	'content' => $content,
	'title' => $title,
	'filter' => $filter,
));

echo elgg_view_page($title, $body);
