<?php
/**
 * Group timeline
 *
 * @package ElggGraphStats
 */

$view = get_input('view', 'default');
$group_guid = (int) get_input('group_guid');

$group = new ElggGroup($group_guid);
if (!$group->guid) {
	forward();
}

$title = elgg_echo('graphstats:group');

elgg_push_breadcrumb(elgg_echo('graphstats'), "graphs/timestats");
elgg_push_breadcrumb($group->name, $group->getURL());
elgg_push_breadcrumb($title);

elgg_set_page_owner_guid($group_guid);

if($view != 'json'){

	$json_url = elgg_get_site_url() . "graphs/group/$group_guid/?view=json";

	$content = elgg_view('graphs/timeline', array('json_url' => $json_url));

	$body = elgg_view_layout('content', array(
		'content' => $content,
		'title' => $title,
		'filter' => '',
	));

	echo elgg_view_page($title, $body);

} else {

	echo elgg_view('timeline/group', array('group_guid' => $group_guid));

}
