<?php

$type = $vars['type'];
$subtype = $vars['subtype'];
$relative = $vars['relative'];

elgg_load_library('elgg:graphs:timestats');

$timestats = timestats($type, $subtype, $relative);

?>

<table style="position: absolute; left: -9999em; top: -9999em;" id="data">
	<tfoot>
		<tr>
		<?php
			foreach ($timestats as $time => $stat) {
				$date = date(elgg_echo('friendlytime:date_format'), $time);
				echo "<th>$date</th>\n";
			}		
		?>
		</tr>
	</tfoot>
	<tbody>
		<tr>
		<?php
			foreach ($timestats as $time => $stat) {
				echo "<td>$stat</td>\n";
			}
		?>
		</tr>
	</tbody>
</table>
