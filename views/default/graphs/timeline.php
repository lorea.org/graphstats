<?php

$json_url = $vars['json_url'];
if(empty($json_url)) return true;

elgg_load_js('simile.timeline');

?>
<div id="group-timeline" style="height: 300px; border: 1px solid #aaa"></div>
<script type="text/javascript">
$(function(){
	var eventSource = new Timeline.DefaultEventSource();
	var bandInfos = [
	 Timeline.createBandInfo({
		 width:          "80%", 
		 intervalUnit:   Timeline.DateTime.DAY, 
		 intervalPixels: 200,
		 eventSource:    eventSource,
	 }),
	 Timeline.createBandInfo({
		 width:          "20%", 
		 intervalUnit:   Timeline.DateTime.MONTH, 
		 intervalPixels: 200,
		 eventSource:    eventSource,
		 overview:       true,
	 })
	];
	bandInfos[1].syncWith = 0;
	bandInfos[1].highlight = true;
	tl = Timeline.create(document.getElementById("group-timeline"), bandInfos);
	tl.loadJSON("<?php echo $json_url; ?>", function(json, url) {
		eventSource.loadJSON(json, url);
	});
	
	$().resize(function(){
		if (resizeTimerID == null) {
			resizeTimerID = window.setTimeout(function() {
				resizeTimerID = null;
				tl.layout();
			}, 500);
		}
	});
});
</script>
