<?php
/**
 * All groups listing page navigation
 *
 */

$tabs = array(
	'absolute' => array(
		'text' => elgg_echo('absolute'),
		'href' => 'graphs/timestats?filter=absolute',
		'priority' => 200,
	),
	'relative' => array(
		'text' => elgg_echo('relative'),
		'href' => 'graphs/timestats?filter=relative',
		'priority' => 300,
	),
);

// sets default selected item
if (strpos(full_url(), 'filter') === false) {
	$tabs['absolute']['selected'] = true;
}

foreach ($tabs as $name => $tab) {
	$tab['name'] = $name;

	elgg_register_menu_item('filter', $tab);
}

echo elgg_view_menu('filter', array('sort_by' => 'priority', 'class' => 'elgg-menu-hz'));
