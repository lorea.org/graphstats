<?php

$group_guid = sanitize_int(get_input('group_guid'));

$entities = elgg_get_entities(array('container_guid'=>$group_guid, 'limit'=>0, 'type'=>'object'));
$events = array();

$db_prefix = elgg_get_config('dbprefix');
$river = elgg_get_river(array(
	'limit' => 0,
	'joins' => array("JOIN {$db_prefix}entities e1 ON e1.guid = rv.object_guid"),
	'wheres' => array("(e1.container_guid = $group_guid)"),
));

foreach($river as $item){
	$subject = $item->getSubjectEntity();
	$object = $item->getObjectEntity();

	array_push($events, array(
		'start' => date('c', $item->posted),
		'icon'=> $icon,
		'title' => $object->title,
		'classname' => 'hot_event',
		'description' => elgg_get_excerpt($object->description),
		'durationEvent' => false,
	));
	
}

$data = array(
	'dateTimeFormat'=>'iso8601',
	'events'=>$events,
);

echo json_encode($data);
