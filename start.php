<?php
/**
 * Elgg graph stats.
 *
 * @package ElggGraphStats
 */

elgg_register_event_handler('init', 'system', 'graphstats_init');

function graphstats_init() {
	
	$timestats_path = elgg_get_plugins_path() . 'graphstats/lib/timestats.php';
	elgg_register_library('elgg:graphs:timestats', $timestats_path);
	
	elgg_register_page_handler('graphs', 'graphstats_pagehandler');
	
	// register the timeline's JavaScript
	$timeline_js = elgg_get_simplecache_url('js', 'timeline');
	elgg_register_js('simile.timeline', $timeline_js);
	
	// register the raphael's Javascript
	$raphael_js = elgg_get_simplecache_url('js', 'raphael/raphael');
	elgg_register_js('raphael', $raphael_js, 'head');
	
	// register the raphael analytics's Javascript
	$analytics_js = elgg_get_simplecache_url('js', 'raphael/analytics');
	elgg_register_js('raphael.analytics', $analytics_js, 'head');
	
	// Modifying group activity pagehander
	elgg_register_plugin_hook_handler('route','groups', 'graphstats_setup_title_button');
}

/**
 * Dispatches graphs pages.
 * URLs take the form of
 *  Time stats:      graphs/timestats/[?type=<type>&subtype=<subtype>][&relative=true]
 *  Group network:   graphs/groupnetwork/
 *  Implication:     graphs/implication/
 *  Group timeline:  graphs/group/<guid>/
 * 
 * @param array $page
 * @return NULL
 */
function graphstats_pagehandler($page){
	$graphstats_dir = elgg_get_plugins_path() . 'graphstats/pages/graphstats';
	
	$page_type = $page[0];
	switch ($page_type) {
		case 'timestats':
			include "$graphstats_dir/timestats.php";
			break;
		case 'groupnetwork':
			break;
		case 'implication':
			break;
		case 'group':
			set_input('group_guid', $page[1]);
			include "$graphstats_dir/group.php";
			break;
	}
}

function graphstats_setup_title_button($hook, $type, $params, $return){
	$page_type = $params['segments'][0];
	$group_guid = $params['segments'][1];
	
	if($page_type == 'activity'){
		add_translation(get_current_language(), array('graphs:group' => elgg_echo('timeline')));
		elgg_set_page_owner_guid((int) $group_guid);
		elgg_register_title_button('graphs', 'group');
	}
}
