<?php

function timestats($type, $subtype, $relative = false){
	
	// Start date
	$entities = elgg_get_entities(array('types' => $type,'subtypes'=>$subtype,'limit' => 1,'order_by'=>'e.time_created asc'));
	$firstentity = $entities[0];
	$initial_time = $firstentity->time_created;
	$initial_date = getdate($initial_time);
	$start_month = mktime(0, 0, 0, $initial_date["mon"], 1, $initial_date["year"]);
	$current_date = $start_month;

	// End date
	$last_time = time();
	$last_date = getdate($last_time);

	// Interval (week)
	$interval = 7*24*60*60;


	$total = elgg_get_entities(array('types' => $type,'subtypes'=>$subtype,'limit' => 99999, 'count'=>true));

	$timestats = array();
	while($current_date<$last_time) {
		$count = elgg_get_entities(array(
			'types' => $type,
			'subtypes' => $subtype,
			'limit' => 99999,
			'count' => true,
			'created_time_lower' => $current_date,
			'created_time_upper'=>$current_date+$interval
		));
		if (empty($count))
			$count = 0;
		$accumulated += $count;
		$timestats[$current_date] = $relative? $count : $accumulated;
		$current_date += $interval;
	}
	
	return $timestats;
}

function timestats_setup_sidebar_menu(){
	$tabs = array(
		'user' => array(
			'text' => elgg_echo('item:user'),
			'href' => 'graphs/timestats?type=user',
			'priority' => 200,
		),
		'group' => array(
			'text' => elgg_echo('groups'),
			'href' => 'graphs/timestats?type=group',
			'priority' => 300,
		),
	);

	$db_prefix = elgg_get_config('dbprefix');
	$result = get_data("SELECT * from {$db_prefix}entity_subtypes");

	foreach($result as $row){
		$type = $row->type;
		$subtype = $row->subtype;
		$tabs[$type.':'.$subtype] = array(
			'text' => elgg_echo("item:$type:$subtype"),
			'href' => "graphs/timestats?type=$type&subtype=$subtype",
			'priority' => 400,
		);
	}

	unset($tabs['object:plugin']);
	unset($tabs['object:widget']);

	// sets default selected item
	if (strpos(full_url(), 'type') === false) {
		$tabs['user']['selected'] = true;
	}

	foreach ($tabs as $name => $tab) {
		$tab['name'] = $name;

		elgg_register_menu_item('page', $tab);
	}
}
