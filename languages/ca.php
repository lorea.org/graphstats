<?php
/**
 * Elgg graphstats plugin language pack
 *
 * @package ElggGraphStats
 */

$catalan = array(
	'graphstats:implication' => 'Implicació',
	'graphstats:graphs' => 'Gràfics',
	'graphstats:timestats' => 'Estadístiques a través del temps',
	'graphstats:groupgraph' => 'Xarxa de grups',
	'timeline' => 'Línea temporal',
);

add_translation("ca", $catalan);
?>
