<?php
/**
 * Elgg graphstats plugin language pack
 *
 * @package ElggGraphStats
 */

$spanish = array(
	'graphstats:implication' => 'Implicación',
	'graphstats:graphs' => 'Gráficos',
	'graphstats:timestats' => 'Estadísticas a través del tiempo',
	'graphstats:groupgraph' => 'Red de grupos',
	'timeline' => 'Línea temporal',
);

add_translation("es", $spanish);
?>
