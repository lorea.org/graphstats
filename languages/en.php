<?php
/**
 * Elgg graphstats plugin language pack
 *
 * @package ElggGraphStats
 */

$english = array(
	'graphstats:implication' => 'Implication',
	'graphstats:graphs' => 'Graphs',
	'graphstats:timestats' => 'Stats over time',
	'graphstats:groupgraph' => 'Group network',
	'timeline' => 'Timeline',
);

add_translation("en", $english);
?>
